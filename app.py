# ---- imports
from flask import Flask, request

from tools.constants import INPUT_DIR, OUTPUT_DIR
from app_tools.constants import README_FILE

from app_controllers.index import _index_controller
from app_controllers.help import _help_controller
from app_controllers.generate_inputs import _generate_inputs_controller
from app_controllers.download_archive import _download_archive_controller

# ---- init variables

app = Flask(__name__)
smartlyExportToolDirPath = "./smartly_export_tool/"
inputDirPath = "./" + INPUT_DIR + "/"
outputDirPath = "./" + OUTPUT_DIR + "/"

# ---- routes


@app.route("/")
def index():
    return _index_controller()


@app.route("/readme")
def readme():
    return _help_controller("./" + README_FILE)

'''
@app.route("/generate_inputs")
def generate_inputs():
    return _generate_inputs_controller(smartlyExportToolDirPath + "main.js", smartlyExportToolDirPath + "config/config.json")


# /download_archive?name=<string:zip_name>&version=<string:version>
@app.route("/download_archive")
def download_archive():
    name = request.args.get("name")
    version = request.args.get("version")
    return _download_archive_controller(name, version, inputDirPath, outputDirPath)'''