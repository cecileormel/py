## ---- imports

import json
import os
import shutil

from datetime import date

from tools.enums import MsgType
from tools.constants import JS_EXTENSION, ZIP_EXTENSION, JSON_EXTENSION

# ---- init functions


def _print(messageType: MsgType, message, param=''):
    type = ""
    if messageType == MsgType.ERROR:
        type = "ERROR: "
    else:
        if messageType == MsgType.OK:
            type = "OK: "
    if param:
        print(type + message + " %s" % param)
    else:
        print(type + message)


def _clean_dir(dirPath):
    funcDescr = "Directory cleaned"
    if os.path.exists(dirPath) and os.path.isdir(dirPath):
        shutil.rmtree(dirPath)
        _print(MsgType.OK, funcDescr, dirPath)


def _create_dir(dirPath):
    funcDescr = "Directory creation"
    try:
        os.mkdir(dirPath)
    except OSError:
        _print(MsgType.ERROR, funcDescr, dirPath)
    else:
        _print(MsgType.OK, funcDescr, dirPath)


def _generate_output_dir(version, inputDirPath, outputDirPath):
    res = False

    nbInputFiles = _get_nb_files(JSON_EXTENSION, inputDirPath)
    if nbInputFiles > 0:
        _print(MsgType.OK, "Start backup of files")

        # prepare export directory
        _clean_dir(outputDirPath)
        _create_dir(outputDirPath)

        # for all json files in the input dir
        for file in os.listdir(inputDirPath):

            d = None
            if file.endswith(JSON_EXTENSION):
                try:
                    # load data from json file
                    d = _get_data(inputDirPath + file)

                    if d["nodes"]:
                        subFolderPath = outputDirPath + \
                            file.split(JSON_EXTENSION)[0]
                        # create sub folder
                        _create_dir(subFolderPath)
                        # create js files
                        _create_js_files(d, version, subFolderPath)
                    else:
                        _print(MsgType.ERROR, "No data in " + file)

                    res = True
                except OSError:
                    _print(MsgType.ERROR, "Load data from " + file)
    else:
        _print(MsgType.ERROR, "No input file(s)")

    return res


def _create_zip(name, srcDirPath, destZipPath):
    funcDescr = "Create zip"
    try:
        shutil.make_archive(name, 'zip', srcDirPath)
        _print(MsgType.OK, funcDescr, destZipPath + name + ZIP_EXTENSION)
    except OSError:
        _print(MsgType.ERROR, funcDescr, destZipPath + name + ZIP_EXTENSION)


def _get_zip_name(name, version):
    return date.today().isoformat() + '_' + version + '_' + name


def _create_file(name, content, destPath):
    funcDescr = "Create file"
    try:
        filePath = os.path.join(destPath, name)
        f = open(filePath, "w", encoding="utf-8")
        f.write(content)
        f.close()
    except OSError:
        _print(MsgType.ERROR, funcDescr, destPath + name)
    else:
        _print(MsgType.OK, funcDescr, destPath + name)


def _create_js_files(data, version, destDirPath):
    # for each graphs, retrieve each nodes
    for g in data["graphs"]:
        graphObj = g['graph']['graph']
        for n in data["nodes"]:
            nId = n["_id"]
            #print('nodeId ', nodeId)
            # for each NodeJs part
            for lastNodeKey in graphObj['nodes'].keys():
                parentLastNodeKey = -1
                if graphObj['nodes'][lastNodeKey]['data'].get('_id') != None and graphObj['nodes'][lastNodeKey]['data'].get('_id') == nId:
                    parentLastNodeKey = lastNodeKey
                    #print('parentLastNodeKey', parentLastNodeKey)

                    # for each connection, find the right connection from BotSays to NodeJs
                    for rightConnectionKey in graphObj['connections'].keys():
                        parentRightConnectionKey = -1
                        for tmpCon in graphObj['connections'][rightConnectionKey]:
                            if tmpCon.get('id') != None and tmpCon.get('id') == parentLastNodeKey:
                                parentRightConnectionKey = rightConnectionKey
                                #print('parentRightConnectionKey', parentRightConnectionKey)

                                # for each connection, find the left connection from BotSays to Intention
                                for leftConnectionKey in graphObj['connections'].keys():
                                    parentLeftConnectionKey = -1

                                    for tmpCon2 in graphObj['connections'][leftConnectionKey]:
                                        if tmpCon2.get('id') != None and tmpCon2.get('id') == parentRightConnectionKey:
                                            parentLeftConnectionKey = leftConnectionKey
                                            #print('parentLeftConnectionKey', parentLeftConnectionKey)

                                            # for each nodes, retrieve intent name and lang and create file
                                            for node in data["nodes"]:
                                                if nId == node["_id"]:
                                                    # save intent name and lang
                                                    iName = graphObj['nodes'][parentLeftConnectionKey]["data"].get(
                                                        "name") or ""
                                                    iLang = graphObj['nodes'][parentLeftConnectionKey]["data"].get(
                                                        "lang") or ""
                                                    #_debug({"iName": iName, "iLang": iLang})
                                                    formattedFileName = _get_date_and_version(
                                                        version) + '_' + iLang + '_' + iName + JS_EXTENSION
                                                    _create_file(
                                                        formattedFileName, node["data"], destDirPath)


def _get_data(jsonPath):
    funcDescr = "JSON read"
    try:
        with open(jsonPath, encoding="utf8") as jsonFile:
            jsonObject = json.load(jsonFile)
            jsonFile.close()
        _print(MsgType.OK, funcDescr, jsonPath)
        return jsonObject
    except OSError:
        _print(MsgType.ERROR, funcDescr, jsonPath)
        return -1


def _get_date_and_version(version):
    return date.today().isoformat() + '_' + version


def _get_nb_files(fileExtension, dirPath):
    cpt = 0
    for file in os.listdir(dirPath):
        if file.endswith(fileExtension):
            cpt = cpt + 1
    return cpt


def _debug(obj):
    print('###' + str(obj) + '###')
