from enum import Enum

# ---- init enums


class MsgType(Enum):
    ERROR = 0
    OK = 1
