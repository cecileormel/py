# ---- init constants

INPUT_DIR = "input"
OUTPUT_DIR = "export"
JS_EXTENSION = ".js"
JSON_EXTENSION = ".json"
ZIP_EXTENSION = ".zip"