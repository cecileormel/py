# ---- imports
import markdown
import flask as fl
import zipfile
import os
from io import BytesIO

from tools.constants import ZIP_EXTENSION

# ---- init functions


def _read_markdown(mdPath):
    file = open(mdPath, mode="r", encoding="utf-8")
    return markdown.markdown(file.read())


def _download_archive(name, dirPath):
    memory_file = BytesIO()
    with zipfile.ZipFile(memory_file, 'w', zipfile.ZIP_DEFLATED) as zipf:
        for root, dirs, files in os.walk(dirPath):
            for file in files:
                zipf.write(os.path.join(root, file))
    memory_file.seek(0)
    return fl.send_file(
        memory_file,
        attachment_filename=name + ZIP_EXTENSION,
        as_attachment=True
    )
