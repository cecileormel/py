# README #

## Pré-requis
- Installation de NodeJS (v.14)
- Installation de Python (v.3.9)
- Lancer les commandes NodeJS en Administateur

## Générer les fichiers JSON de Smartly dans le répertoire `inputs/`
Se placer dans le répertoire `smartly_export_tool/` et taper la commande : 
> `npm install`

Puis taper la commande :
> `npm run export`

## Lancer le script Python
Se placer à la racine du projet et taper la commande : 
> `python script.py <VERSION> <ZIP_NAME>`

#### Définitions des paramètres
- `<VERSION>` : Version du sprint
- `<ZIP_NAME>` : Nom du zip exporté

#### Exemple de commande
`python script.py SP27 NodeJS`