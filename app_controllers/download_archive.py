# ---- imports

from tools.functions import _generate_output_dir
from app_tools.functions import _download_archive


# ---- init controller


def _download_archive_controller(name, version, inputDirPath, outputDirPath):
    res = _generate_output_dir(version, inputDirPath, outputDirPath)
    if res:
        return _download_archive(name, outputDirPath)
    else:
        return "Export from JSON file(s) failed"