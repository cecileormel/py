# ---- imports

from app_tools.functions import _read_markdown

# ---- init controller


def _help_controller(readMePath):
    return _read_markdown(readMePath)
