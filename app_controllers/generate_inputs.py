# ---- imports

from subprocess import call
from tools.functions import _get_data

# ---- init controller


def _generate_inputs_controller(nodeScriptPath, configPath):
    # load data from json file
    d = _get_data(configPath)

    executions = []
    skill_ids = d.get('input').get('skill_id')

    if skill_ids and len(skill_ids) > 0:
        for item in skill_ids:
            call(["node", nodeScriptPath, item.get("id")])
            executions.append(item.get('bot'))
        res = "JSON files generated for bots : " + str(executions)

    else:
        res = "There is an issue with the JSON config file."
    return str(res)
