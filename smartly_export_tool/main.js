"use strict";

var Sequence = require("sequence").Sequence;
var serviceInput = require("./services/input.service");
const config = require("./config/config.json");
var fs = require("fs");
var idx = "1";

process.argv.forEach((val, index) => {
  if (index == 2) {
    //console.log(`${index}: ${val}`);
    idx = `${val}`;
  }
});

let date_ob = new Date();
let date = ("0" + date_ob.getDate()).slice(-2);
let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
let year = date_ob.getFullYear();
let hours = ("0" + date_ob.getHours()).slice(-2);

var skillId = "";
var botName = "";
for (var key in config.input.skill_id) {
  if (config.input.skill_id[key].id == idx) {
    skillId = config.input.skill_id[key].skill;
    botName = config.input.skill_id[key].bot;
    console.log(idx, '// bot : ', botName, '// skillId : ', skillId);
  }
}

// Clone app form platform A into platform B
var sequence = Sequence.create();
sequence
  .then(function (next) {
    // Get data from skill, graphs, nodes, intents and slots from the first platform A
    serviceInput
      .gatherInput(idx)
      .then((input) => {
        next(input)
      })
      .catch((err) => {
        console.error("ERROR: while gathering input ", err);
      });
  })
  .then(function (next, input) {
    let json = JSON.stringify(input);
    let filePath = __dirname.replace('smartly_export_tool', 'input/') + year + month + date + hours + "-" + skillId + "-" + botName + ".json";
    fs.writeFile(filePath, json, "utf8", function (err) {
      if (err) {
        return console.log(err);
      }
      console.log("OK: File saved " + filePath);
    });
  });
