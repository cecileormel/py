"use strict";

const rpn = require("request-promise-native");
const config = require("../config/config.json");
var _ = require("lodash");

var gatherInput = (idx, request) => {
  var skillId = "";
  var botId = "";
  for (var key in config.input.skill_id) {
    if (config.input.skill_id[key].id == idx) {
      skillId = config.input.skill_id[key].skill;
      botId = config.input.skill_id[key].bot;
      console.log(idx, skillId, botId);
    }
  }
  return new Promise((resolve, reject) => {
    const input = { skill: null, graphs: null, intents: null, entities: null, nodes: null };
    var uril = `${config.input.api_uri}/skills/${skillId}`;
    console.log("GET " + uril);
    // Get Skill
    rpn({
      method: "GET",
      uri: `${config.input.api_uri}/skills/${skillId}`,
      headers: {
        Authorization: `Bearer ${config.input.token}`,
      },
      json: true,
    })
      .then((data) => {
        input.skill = data.skill;
        input.skill["collaborators"] = [];
        input.skill["slave_bots"] = [];
        // Get graphs
        let promises = Object.values(input.skill.graph).map((graph_id) =>
          rpn({
            method: "GET",
            uri: `${config.input.api_uri}/graphs/${graph_id}/skills/${skillId}`,
            headers: {
              Authorization: `Bearer ${config.input.token}`,
            },
            json: true,
          }).then((data) => data)
        );
        return Promise.all(promises);
      })
      .then((data) => {
        input.graphs = data;
        // Get nodes
        let promises = [];
        input.graphs.forEach((item) => {
          let graph = item.graph.graph;
          _.mapValues(graph.nodes, (node) => {
            if (node.type == "code" || node.type == "messenger-text") {
              if (typeof node.data !== "undefined" && typeof node.data._id !== "undefined") {
                let node_id = node.data._id;
                promises.push(
                  rpn({
                    method: "GET",
                    uri: `${config.input.api_uri}/nodes/${node_id}`,
                    headers: {
                      Authorization: `Bearer ${config.input.token}`,
                    },
                    json: true,
                  }).then((data) => {
                    var returnObj = {
                      _id: data.node["_id"],
                      skill_id: data.node["skill_id"],
                      data: data.node["data"],
                    };
                    return returnObj;
                  })
                );
              }
            }
          });
        });
        return Promise.all(promises);
      })
      .then((data) => {
        input.nodes = data;
        // Get FAQ
        let promises = [];
        input.graphs.forEach((item) => {
          let id = item.graph._id;
          let lang = item.graph.lang;
          promises.push(
            rpn({
              method: "GET",
              uri: `${config.input.api_uri}/knowledges/skill/${skillId}/external?lang=${lang}`,
              headers: {
                Authorization: `Bearer ${config.input.token}`,
              },
              json: true,
            }).then((data) => data.faq)
          );
        });
        return Promise.all(promises);
      })
      .then((data) => {
        input.faq = data;
        // Get intents
        let intents = [];
        let promises = [];
        input.graphs.forEach((item) => {
          let graph = item.graph;
          graph.intents.forEach((intent_id) => {
            promises.push(
              rpn({
                method: "GET",
                uri: `${config.input.api_uri}/intents/${intent_id}`,
                headers: {
                  Authorization: `Bearer ${config.input.token}`,
                },
                json: true,
              }).then((data) => {
                var returnObj = {
                  _id: data.intent["_id"],
                  entity_id: data.intent["entity_id"],
                  slots: data.intent["slots"],
                  name: data.intent["name"],
                  lang: data.intent["lang"],
                };
                return returnObj;
              })
            );
          });
        });
        return Promise.all(promises);
      })
      .then(async function (data) {
        input.intents = data;
        // Get entities
        let promises = [];
        for (let intent of data) {
          for (let entity_id of Object.values(intent.slots)) {
            promises.push(
              rpn({
                method: "GET",
                uri: `${config.input.api_uri}/slots/${entity_id}`,
                headers: {
                  Authorization: `Bearer ${config.input.token}`,
                },
                json: true,
              })
                .then((data) => data.slot)
                .catch((err) => {
                  // TODO : If slot has type system and doesn't belong to this user !!!!
                })
            );
          }
        }
        input.entities = await Promise.all(promises);
        resolve(input);
      })
      .catch((err) => {
        console.log(err.statusCode, err.error);
        reject(err.error);
      });
  });
};

module.exports = {
  gatherInput: gatherInput,
};
