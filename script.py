## ---- imports
import sys
import os

from tools.enums import MsgType
from tools.constants import INPUT_DIR, OUTPUT_DIR
from tools.functions import _print, _create_zip, _generate_output_dir

# ---- init variables

inputDirPath = os.getcwd() + "/" + INPUT_DIR + "/"
exportDirPath = os.getcwd() + "/" + OUTPUT_DIR + "/"

# ---- main script

if len(sys.argv) != 3:
    _print(MsgType.ERROR, "One or more params are missing, use this order: <VERSION> <ZIP_NAME>")
else:
    VERSION = sys.argv[1]
    ZIP_NAME = sys.argv[2]

    res = _generate_output_dir(VERSION, inputDirPath, exportDirPath)
    if res:
        _create_zip(ZIP_NAME, exportDirPath, './')
    else:
        _print(MsgType.ERROR, "Export from JSON file(s) failed")