# README #

## Pré-requis
- Installation de NodeJS (v.14)
- Installation de Python (v.3.9)
- Installation du package Flask pour Python `pip3 install flask`
- Installation du package Markdown pour Python `pip3 install markdown`

## Lancer l'application Python (envt local)
Taper la commande : 
> `python app.py`

## Fonctionnalités de l'application Python (envt local)
Différentes fonctionnalités sont disponibles, voici les URLs accessibles via le navigateur :
> - Accueil : [http://127.0.0.1:8080/](http://127.0.0.1:8080/)

> - Aide : [http://127.0.0.1:8080/readme](http://127.0.0.1:8080/readme)

> - Obtenir les NodeJS des bots Smartly :

>> 1. Générer les fichiers JSON de Smartly dans le répertoire `inputs/` à l'aide de l'URL suivante et attendre le message de succès dans le navigateur : [http://127.0.0.1:8080/generate_inputs](http://127.0.0.1:8080/generate_inputs)

>> 2. Générer les fichiers en sortie, les archiver et télécharger l'archive à l'aide de l'URL suivante (en adaptant les 2 paramètres en MAJUSCULES) : [http://127.0.0.1:8080/download_archive?name=ZIP_NAME&version=VERSION](http://127.0.0.1:8080/download_archive?name=<ZIP_NAME>&version=<VERSION>)

>>>> #### Définitions des paramètres
>>>> - `<VERSION>` : Version du sprint
>>>> - `<ZIP_NAME>` : Nom du zip créé à la volée